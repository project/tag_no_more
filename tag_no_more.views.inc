<?php

/**
 * @file
 * Views include file for the Tag No More module.
 */

/**
 * Implements hook_views_data().
 */
function tag_no_more_views_data() {

    $data['tag_no_more']['table']['group'] = t('Tag No More');

    $data['tag_no_more']['table']['join'] = array(
        'taxonomy_term_data' => array(
            'left_field' => 'tid',
            'field' => 'tid',
        ),
    );

    $data['tag_no_more']['tag_no_more'] = array(
        'title' => t('Tag No More'),
        'help' => t('Whether or not this taxonomy term is set to "Tag No More".'),
        'field' => array(
            'handler' => 'views_handler_field_boolean',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_boolean_operator',
            'label' => t('Tag No More'),
            'type' => 'yes-no',
            'use equal' => TRUE,
        ),
        'sort' => array(
            'handler' => 'views_handler_sort',
        ),
    );

    return $data;
}
